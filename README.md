## ------------ Welcome to SAHAKKA FrontEnd team --------------
## Our work
We divided our project for 3 parts (First-Page , Freelancer and Business-Owner) 
## First-Page (/)
- Home Page : 100%
- About Us : 100%
- Login : 100%
- Sign Up (Freelancer) : 100%
- Sign Up (Business or Client) : 100%
- Learn More Freelancer : 100%
- Learn More Freelancer Business Owner : 100%
- Chat System : On Processing
## Business Owner (/business)  
- Feed : 100%
- Project detail : 100%
- Business-Owner Profile : 100%
- Project Post : 100%
- Edit Profile 4 pages : 100%
- When Search for the freelancer : 100%
- Service Card Detail : 100%
- Freelancer Profile Detail : 100% 
## Freelancer (/freelancer) 
- Freelancer- Feed : 100%
- BussinessOwner Post Detail : 100%
- Business-Owner Profile : 100%
- Freelancer Profile Detail : 100%
- Service Card Detail : 100%
- Post Service Card : 100%
- Edit Profile information 5 pages : 100% 
## Other
- Localization : 20% 
