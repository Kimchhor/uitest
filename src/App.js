import React from 'react'
import './App.css';
import { BrowserRouter as Router , Route , Switch } from 'react-router-dom'
import Freelancer from './views/freelancer';
import BusinessOwner from './views/BusinessOwner';
import FirstPage from './views/firstPage';


function App() {
  return (
    <>
    <Router>
      <Switch>
        <Route path="/business">
          <BusinessOwner/>
        </Route>
        <Route path="/freelancer">
          <Freelancer />
        </Route>
        <Route path="/">
          <FirstPage/>
        </Route>    
      </Switch>
    </Router>
    </>
  );
}

export default App;
