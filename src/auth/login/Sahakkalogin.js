import React, { useState } from "react";
import { Row, Col, Container } from "react-bootstrap";
import "./styles.css";
import { Form, Input, Button } from "antd";
import { MailOutlined, LockOutlined} from "@ant-design/icons";
import LoginWithFacebook from "./LoginWithFacebook";
import LoginWithGoogle from './LoginWithGoogle'

const Sahakkalogin = () => {
  const [formLayout] = useState("vertical");
  const formItemLayout =
    formLayout === "vertical"
      ? {
          labelCol: {
            span: 8,
          },
          wrapperCol: {
            span: 30,
          },
        }
      : null;
  return (
    <div>
      <Container>
        <Row className="log-in">
          <Col lg={7} md={7} sm={12} xs={12} className="left-side">
            <h2 className="mb-4">Login</h2>
            <p>
                Welcome Back!<br/> Hope you will have fun exploring the the
                freelancers and the projects that you desired and enjoy the
                wonderful experiences .....
            </p>
            <img src="./assets/auth_img/login.png" className="log-pic"></img>
          </Col>
          <Col lg={5} md={5} sm={12} xs={12} className="right-side">
            <Form
              initialValues={{ remember: true }}
              {...formItemLayout}
              layout={formLayout}
            >
              <Form.Item
                label="Email"
                name="email"
                rules={[
                  { required: true, message: "Please input your Email!" },
                ]}
              >
                <Input placeholder="Email" prefix={<MailOutlined />} />
              </Form.Item>
              <Form.Item
                label="Password"
                name="password"
                rules={[
                  { required: true, message: "Please input your password!" },
                ]}
              >
                <Input.Password
                  placeholder="Password"
                  prefix={<LockOutlined />}
                />
              </Form.Item>
              <a href="#" className="forgotPassword">Forgot password?</a>
              <Form.Item>
                <a href="#">
                  <Button type="default" shape="round" htmlType="submit">
                    Log In
                </Button></a>
              </Form.Item>
              {/* --------fb, google login-------- */}
              <br/>
              <LoginWithFacebook /><br/>
              <div className="google-log">
                <LoginWithGoogle/>  
              </div>
            </Form>
          </Col>
        </Row>
      </Container>
    </div>
  );
};

export default Sahakkalogin;