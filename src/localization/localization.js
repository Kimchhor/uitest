import LocalizedStrings from 'react-localization';

export const strings = new LocalizedStrings({
    en: {
        overview: "Overview",
        project: "Project",
        freelancer: "Freelancer",
        aboutUs: "About Us",
        search: "Search",
        dashboard: "Dashboard",
        postProject: "Post Project",
        categories: "Categories",
        more: "More...",
        title: "English",
        message: "Message",
        login: "Login",
        register: "Register",
        myfeeds: "My Feeds",
        postService: "Post Service"
    },
    km: {
        overview: "ទិដ្ឋភាពទូទៅ",
        project: "គម្រោង",
        freelancer: "អ្នកឯករាជ្យ",
        aboutUs: "អំពី​ពួក​យើង",
        search: "ស្វែងរក",
        dashboard: "ផ្ទាំងគ្រប់គ្រង",
        postProject: "ផុសគម្រោង",
        categories: "ប្រភេទ",
        more: "ផ្សេងៗ",
        title: "ភាសាខ្មែរ",
        message: "សារ",
        login: "ចូលប្រើ",
        register: "ចុះឈ្មោះ",
        myfeeds: "មតិព័ត៌មានរបស់ខ្ញុំ",
        postService: "ផុសសេវាកម្ម"
    }
});