import React from 'react'
import './styles.css'
import { Col, Container, Row } from 'react-bootstrap'
import { Link } from 'react-router-dom'

export default function Footer() {
    return (
        <div className="footer">
            <Container>
                <Row>
                    <Col lg={4} md={4}>
                        <div className="d-flex">
                            <img src="./logo-footer.png" alt="Logo"/>
                            <p className="text">Explore  your desired projects and find the qualified freelancers . SAHAKKA is your choice forever.</p>
                        </div>
                    </Col>
                    <Col lg={2} md={2}>
                        <h6>SAHAKKA</h6>
                        <ul>
                            <li><Link to='/aboutus'>About Us</Link></li>
                            <li><Link to='#'>Terms of Service</Link></li>
                            <li><Link to='#'>Private Policy</Link></li>
                        </ul>
                    </Col>
                    <Col lg={2} md={2}>
                        <h6>DISCOVER</h6>
                        <ul>
                            <li><Link to='#'>Freelancers</Link></li>
                            <li><Link to='#'>Projects</Link></li>
                        </ul>
                    </Col>
                    <Col lg={2} md={2}>
                        <h6>SUPPORT</h6>
                        <ul>
                            <li><Link to='#'>FQA</Link></li>
                        </ul>
                    </Col>
                    <Col lg={2} md={2}>
                        <h6>CONTACT US</h6>
                        <ul className="contact-us">
                            <li>
                                <img src="./assets/freelancer_pro/fb.png" alt="facebook" /> Facebook
                            </li>
                            <li>
                                <img src="./assets/freelancer_pro/linkin.png" alt="linkIn" /> Link In
                            </li>
                            <li>
                                <img src="./assets/freelancer_pro/telegram.png" alt="telegram" /> Telegram
                            </li>
                            <li>
                                <img src="./assets/freelancer_pro/twiter.png" alt="twitter" /> Twitter
                            </li>
                        </ul>
                    </Col>
                </Row>
            </Container>
            <div className="b-footer">
                © 2021 Copyright SAHAKKA
            </div>
        </div>
    )
}
