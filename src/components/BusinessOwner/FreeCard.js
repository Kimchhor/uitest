import React from 'react'
import { Card } from 'antd'
import { StarFilled } from '@ant-design/icons'
import { NavLink } from 'react-router-dom'


function FreeCard() {
    return (
        <div className="free-card">
            <Card
                hoverable
                style={{ width: '100%' }}
                cover={<img alt="cover" src="./assets/business_img/free-card.png" />}
            >
                    <div className="d-flex">
                        <img alt="freelancer cover" src="./assets/freelancer_pro/menghong.png" height="49px"/>
                        <div className="ms-3">
                            <h6>Kay Keo</h6>
                            <p style={{fontSize:'12px',marginTop:'-3px'}}>Full Stack Developer</p>
                        </div>
                    </div>
                    <NavLink to="/freelancer_card_detail">
                        <p style={{fontSize:'12px'}}>
                            I will design fullstack application in a reasonable prize. Feel free to reach out in case you are in interested or having any questions.
                        </p>
                    </NavLink>
                    <p><StarFilled style={{color:'#fbd98d',fontSize:'15px',position:'absolute',marginTop:'2px'}}/> <span style={{marginLeft:'20px'}}>4.5(120)</span></p>
            </Card>
        </div>
    )
}

export default FreeCard
