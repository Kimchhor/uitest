import React, {useState} from 'react'
import './styles.css'
import { GlobalOutlined } from '@ant-design/icons'
import { Navbar,Nav,Form,FormControl,Button,NavDropdown } from 'react-bootstrap'
import {NavLink} from 'react-router-dom'
import { strings } from '../../localization/localization';

function NavBar() {
    const [language, setLanguage] =	useState ([
		{
			title: "English",
			key: "en"
		},
		{
			title: "Khmer",
			key: "km"
		}
	])
    const [update, setUpdate] = useState({})
	const change = (lang) => {
		strings.setLanguage(lang)
		setUpdate({})
        // localStorage.setItem("lang")
	}
    return (
        <div className="header">
            <Navbar bg="light" expand="lg">
                <Navbar.Brand as={NavLink} to="/">
                    <img src="./logo.png" alt="no logo show" />
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="navbarScroll" />
                <Navbar.Collapse id="navbarScroll" >
                    <Form className="d-flex">
                        <FormControl                   
                            type="search"
                            placeholder={strings.search}
                            className="mr-2"
                            aria-label="Search"
                        />
                        <Button variant="outline-success">{strings.search}</Button>
                    </Form>
                        <Nav
                        className="mr-auto my-2 my-lg-0"
                        style={{ maxHeight: '100px' }}
                        navbarScroll
                        >
                            <Nav.Link as={NavLink} to="/" exact={true}>{strings.dashboard}</Nav.Link>                       
                            <NavDropdown title={strings.categories} id="basic-nav-dropdown">
                                <NavDropdown.Item href="#action/3.1">Web & Mobile Design</NavDropdown.Item>
                                <NavDropdown.Item href="#action/3.2">BackEnd Developement</NavDropdown.Item>
                                <NavDropdown.Item href="#action/3.3">Game Design</NavDropdown.Item>
                                <NavDropdown.Item href="#action/3.3">Logo Design</NavDropdown.Item>
                                <NavDropdown.Item href="#action/3.3">Android Application Developement</NavDropdown.Item>
                                <NavDropdown.Divider />
                                <NavDropdown.Item href="#action/3.4">{strings.more}</NavDropdown.Item>
                            </NavDropdown>
                            <Nav.Link as={NavLink} to="/post_project">{strings.postProject}</Nav.Link>
                            <Nav.Link>{strings.message}</Nav.Link>
                        </Nav>
                </Navbar.Collapse>
                <Nav>
                <span><GlobalOutlined style={{fontSize:'20px',position:'absolute',marginLeft:'0px', marginTop:'12px'}}/></span>
                <NavDropdown title={strings.title} id="basic-nav-dropdown">
							{
								language.map((lang, index) =>
									<NavDropdown.Item key={index} onClick={()=>change(lang.key)}>
										{lang.title}
									</NavDropdown.Item>
								)
							}
						</NavDropdown>
                    <NavLink to="/profile_detail">
                        <img className="me-2" src="./assets/business_img/keo.png" alt=".." height="36px" />
                    </NavLink>
                </Nav>
            </Navbar>
        </div>
    )
}

export default NavBar
