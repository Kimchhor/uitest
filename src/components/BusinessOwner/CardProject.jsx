import React from 'react';
import { Button } from 'react-bootstrap';
import { NavLink } from 'react-router-dom';
import './styles.css';

const CardProject = () => {
    return (
        <div className="pro-card d-flex mt-2">
            <img src="./assets/business_img/card-img.png" alt="no card show" width="100%"/>
            <div className="ms-4 pb-3 pt-1">
                <NavLink to="./project_detail">
                    <p className="post-title"><b>Looking for qualified developer who can work on the blockchain applications Responsive designs to all screens</b></p>
                </NavLink>
                <Button variant="warning">Web Development</Button>{' '}
                <Button variant="warning">Backend Development</Button>
                <NavLink to="./project_detail">
                    <p className="mt-3 description">Responsive designs to all screens (Laptops, Notebooks, Tablets, and Mobiles).
                        Custom Eye-catching design.
                        SEO Friendly design.
                        Optimized Images for Fast-Load Optimization Minimum HTML (No unnecessary Divs).
                        Image sliders.......................</p>
                </NavLink>
                <Button size="sm" className="button-action-style" variant="outline-dark">
                    Update</Button>
                <Button size="sm" className="button-action-style mx-4 pending" variant="outline-dark">
                    Pending</Button>
                <Button size="sm" className="button-action-style" variant="outline-dark">
                    Delete</Button>
            </div>
        </div>
    );
};

export default CardProject;