import React from 'react'
import { Col, Row ,Button} from 'react-bootstrap'
import {StarFilled} from '@ant-design/icons'
import './styles.css'
import { NavLink } from 'react-router-dom'

export default function Card() {
    return (
        <div className="card-service">
            <Row>
                <Col lg={5} md={5} sm={5} >
                    <div className="cover-post">
                        <img src="./assets/freelancer_pro/card.png" alt=".."/>
                    </div>
                </Col>
                <Col lg={7} md={7} sm={7}>
                    <p className="free-card-title">
                        <NavLink to="/detail"><b>I will design fullstack application in a reasonable prize. Feel free to reach out in case you are in interested or having any questions</b></NavLink>
                    </p>
                        <p className="free-card-des">
                            Hello. Thank for coming here . Here are the services that will be cover upon taking the services First Service is to build a backend using these technologies  you are in interested or having any questions
                        </p>
                    <div>
                        <StarFilled style={{color:'#fbd98d',fontSize:'15px',position:'absolute',marginTop:'2px'}}/> <span style={{marginLeft:'20px'}}>4.7(210)</span>
                    </div>
                    <div className="mt-3">
                        <Button variant="secondary">Update</Button>
                        <Button variant="secondary">Delete</Button>
                        <Button variant="secondary" className="pending">Pending</Button>
                    </div>
                </Col>
            </Row>
        </div>
    )
}
