import React from 'react'
import {Card,Button} from 'react-bootstrap'
import './styles.css'
import { Carousel } from 'antd'
import { LikeOutlined , CommentOutlined} from '@ant-design/icons'
import { NavLink } from 'react-router-dom'



function BsCard() {
    return (
        <div className="bs-card">
            <Card>
                <Carousel>
                    <div>
                        <div className="card-cover">
                            <img src="./assets/freelancer_pro/feed_cover.png" alt="..." width="100%"/>
                        </div>
                    </div>
                    <div>
                        <div className="card-cover"><img src="./assets/freelancer_pro/feed_cover.png" alt="..." width="100%" /></div>
                    </div>
                    {/* <div>
                        <h3 style={contentStyle}><img src="./assets/freelancer_pro/feed_cover.png" alt="..." width="100%" /></h3>
                    </div>
                    <div>
                        <h3 style={contentStyle}><img src="./assets/freelancer_pro/feed_cover.png" alt="..." width="100%" /></h3>
                    </div> */}
                </Carousel>
                <Card.Body>
                    <Card.Title>
                        <div className="d-flex mb-2">
                            <img className="me-4" src="./assets/freelancer_pro/kimchhor.png" height="46px" alt=".."/>
                            <h6>Chiv Kimchhor <br/><span style={{fontSize:'12px',fontWeight:'400'}}>Posted : a moment ago</span></h6>
                        </div>
                        <p className="bs-card-title">
                            <NavLink to="/bs_card_detail">
                                Looking for  fullstack developer who can work on the blockchain, front end and back end who can work on the blockchain.
                            </NavLink>
                        </p>
                    </Card.Title>
                    <Button variant="warning">Web Development</Button>{' '}<Button variant="warning">Backend Development</Button>
                    <ul className="bs-card-des">
                        <li>Responsive designs to all screens (Laptops, Notebooks, Tablets, and Mobiles).</li>
                        <li>Custom Eye-catching design.</li>
                        <li>SEO Friendly design.</li>
                        <li>Optimized Images for Fast-Load Optimization Minimum HTML (No unnecessary Divs).</li>
                        <li>Image sliders Fast-Load Optimization Minimum HTM</li>
                    </ul>
                    <div className="mt-4">
                        <span className="like"><LikeOutlined style={{verticalAlign:'initial'}}/> 100 Likes</span>
                        <span className="comment"><CommentOutlined style={{verticalAlign:'initial'}}/> 120 comments</span>
                        <span className="deadline">Deadline<br/>21 Feb 2022 </span>
                    </div>
                </Card.Body>
            </Card>
        </div>
    )
}

export default BsCard
