import React,{ useState } from 'react'
// import './freelancer/styles.css'
import { GlobalOutlined } from '@ant-design/icons'
import { Navbar,Nav,Form,FormControl,Button, NavDropdown } from 'react-bootstrap'
import { NavLink } from 'react-router-dom'
import { strings } from '../../localization/localization';

function NavMenu() {
    const [language, setLanguage] =	useState ([
		{
			title: "English",
			key: "en"
		},
		{
			title: "Khmer",
			key: "km"
		}
	])
    const [update, setUpdate] = useState({})
	const change = (lang) => {
		strings.setLanguage(lang)
		setUpdate({})
        // localStorage.setItem("lang")
	}
    return (
        <div className="header">
            
            <Navbar bg="light" expand="lg">
                <Navbar.Brand as={NavLink} to ="/">
                    <img src="./logo.png" alt="no logo show" />
                </Navbar.Brand>
                <Navbar.Toggle aria-controls="navbarScroll" />
                <Navbar.Collapse id="navbarScroll" >
                    <Form className="d-flex">
                        <FormControl                   
                            type="search"
                            placeholder={strings.search}
                            className="mr-2"
                            aria-label="Search"
                        />
                        <Button variant="outline-success">{strings.search}</Button>
                    </Form>
                        <Nav
                        className="mr-auto my-2 my-lg-0"
                        style={{ maxHeight: '100px' }}
                        navbarScroll
                        >
                            <Nav.Link as={NavLink} to ="/" exact={true}>{strings.overview}</Nav.Link>
                            <Nav.Link>{strings.project}</Nav.Link>
                            <Nav.Link>Freelancer</Nav.Link>
                            <Nav.Link as={NavLink} to ="/aboutus">{strings.aboutUs}</Nav.Link>
                        </Nav>
                </Navbar.Collapse>
                <Nav>
                <span><GlobalOutlined style={{fontSize:'20px',position:'absolute',marginLeft:'0px', marginTop:'12px'}}/></span>
                <NavDropdown title={strings.title} id="basic-nav-dropdown">
							{
								language.map((lang, index) =>
									<NavDropdown.Item key={index} onClick={()=>change(lang.key)}>
										{lang.title}
									</NavDropdown.Item>
								)
							}
						</NavDropdown>
                    <div className="mt-1">
                        <NavLink to="/login">
                            <Button variant="outline-success" className="login">{strings.login}</Button>
                        </NavLink>
                        <NavLink to="/signup">
                            <Button variant="outline-success" className="register">{strings.register}</Button>
                        </NavLink>
                    </div>
                </Nav>
                
            </Navbar>
            
        </div>
    )
}

export default NavMenu
