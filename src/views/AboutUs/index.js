import React from 'react'
import './styles.css'
import { Container, Row ,Col} from 'react-bootstrap'
import "owl.carousel/dist/assets/owl.carousel.css";
import "owl.carousel/dist/assets/owl.theme.default.css";

export default function AboutUs() {
    return (
        <div className="first-page about-us">
            <Container>
                <Row className="my-5">
                    <Col lg={3} md={3} sm={12} xs={12}>
                        <img src="./assets/freelancer_pro/logo-about.png" alt="Feed img" width="100%" />
                    </Col>
                    <Col lg={9} md={9} sm={12} xs={12}>
                        <h1 className="my-5">Welcome to<span style={{color:'#ffb000'}}> SAHAKKA</span></h1>    
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla finibus, orci in facilisis efficitur, tellus sem malesuada nisi, eget laoreet sapien mauris quis augue. Quisque tincidunt neque vitae arcu tincidunt semper. Morbi bibendum, ipsum in ultrices sollicitudin, nulla odio viverra tortor, in convallis nulla metus sed odio.</p>
                    </Col>
                </Row>
                <Row className="mb-5">
                    <Col lg={5} md={5} sm={12} xs={12}>
                        <img src="./assets/freelancer_pro/about2.png" alt="Feed img" width="100%" height="250vh"/>
                    </Col>
                    <Col lg={7} md={7} sm={12} xs={12}>
                        <div className="p-5 pt-0">
                            <h3 className=" mb-2">Our Story</h3> 
                            <div className=" mb-5 cus-border-button"></div>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla finibus, orci in facilisis efficitur, tellus sem malesuada nisi, eget laoreet sapien mauris quis augue. Quisque tincidunt neque vitae arcu tincidunt semper. Morbi bibendum, ipsum in ultrices sollicitudin, nulla odio viverra tortor, in convallis nulla metus sed odio. Maecenas mattis tortor ac facilisis facilisis. Pellentesque efficitur neque nec enim scelerisque, at dignissim libero scelerisque. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            </p>
                        </div>
                    </Col>
                </Row>
                <Row className="mb-5">
                    <Col lg={7} md={7} sm={12} xs={12}>
                        <div className="p-5 pt-0 ps-0">
                            <h3 className=" mb-2">Our Mission</h3> 
                            <div className=" mb-5 cus-border-button"></div>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla finibus, orci in facilisis efficitur, tellus sem malesuada nisi, eget laoreet sapien mauris quis augue. Quisque tincidunt neque vitae arcu tincidunt semper. Morbi bibendum, ipsum in ultrices sollicitudin, nulla odio viverra tortor, in convallis nulla metus sed odio. Maecenas mattis tortor ac facilisis facilisis. Pellentesque efficitur neque nec enim scelerisque, at dignissim libero scelerisque. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            </p>
                        </div>
                    </Col>
                    <Col lg={5} md={5} sm={12} xs={12}>
                        <img src="./assets/freelancer_pro/about-mission.png" alt="Feed img" width="100%" height="250vh"/>
                    </Col>
                </Row>
                <Row className="mb-5">
                    <Col lg={5} md={7} sm={12} xs={12}>
                        <img src="./assets/freelancer_pro/about-vision.png" alt="Feed img" width="100%" height="250vh"/>
                    </Col>
                    <Col lg={7} md={7} sm={12} xs={12}>
                        <div className="p-5 pt-0">
                            <h3 className=" mb-2">Our Vision</h3> 
                            <div className=" mb-5 cus-border-button"></div>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla finibus, orci in facilisis efficitur, tellus sem malesuada nisi, eget laoreet sapien mauris quis augue. Quisque tincidunt neque vitae arcu tincidunt semper. Morbi bibendum, ipsum in ultrices sollicitudin, nulla odio viverra tortor, in convallis nulla metus sed odio. Maecenas mattis tortor ac facilisis facilisis. Pellentesque efficitur neque nec enim scelerisque, at dignissim libero scelerisque. Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                            </p>
                        </div>
                    </Col>
                </Row>
                <div className="text-center">
                    <h2 className="our-team-header">Our Team</h2> 
                </div>
                <div className="our-team">
                    <Row className="mb-5">
                        <Col lg={4} md={4} sm={12} xs={12}>
                            <img src="./assets/our-team/keo.png" style={{height:'40vh'}} alt="Feed img" />
                        </Col>
                        <Col lg={8} md={8} sm={12} xs={12}>
                            <h3 className=" mb-2" style={{color:'#ffb000'}}>Kay Keo</h3> 
                            <b className="mb-3">PROJECT LEADER</b>
                            <div className=" mt-3 cus-border-button-team"></div>
                            <div className="social-media mt-3">
                                <img className="px-2" src="./assets/freelancer_pro/fb.png" alt="facebook" />
                                <img className="px-2" src="./assets/freelancer_pro/linkin.png" alt="linkIn" /> 
                                <img className="px-2" src="./assets/freelancer_pro/telegram.png" alt="telegram" /> 
                                <img className="px-2" src="./assets/freelancer_pro/twiter.png" alt="twitter" /> 
                            </div>
                            <p className="mt-4">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla finibus, orci in facilisis efficitur, tellus sem malesuada nisi, eget laoreet sapien mauris quis augueLorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla finibus, orci in facilisis efficitur, tellus sem malesuada nisi, eget laoreet sapien mauris quis augue 
                            </p>
                            <hr></hr>
                        </Col>
                    </Row>
                    <Row className="mb-5">
                        <Col lg={8} md={8} sm={12} xs={12}>
                            <div>
                                <h3 className=" mb-2" style={{color:'#ffb000'}}>Leng Chhinghor</h3> 
                                <b className="mb-3">DEVELOPER</b>
                                <div className=" mt-3 cus-border-button-team">
                            </div>
                            <div className="social-media mt-3">
                                <img className="px-2" src="./assets/freelancer_pro/fb.png" alt="facebook" />
                                <img className="px-2" src="./assets/freelancer_pro/linkin.png" alt="linkIn" /> 
                                <img className="px-2" src="./assets/freelancer_pro/telegram.png" alt="telegram" /> 
                                <img className="px-2" src="./assets/freelancer_pro/twiter.png" alt="twitter" /> 
                            </div>
                            <p className="mt-4">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla finibus, orci in facilisis efficitur, tellus sem malesuada nisi, eget laoreet sapien mauris quis augueLorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla finibus, orci in facilisis efficitur, tellus sem malesuada nisi, eget laoreet sapien mauris quis augue 
                            </p>
                            <hr></hr>
                            </div>
                        </Col>
                        <Col lg={4} md={4} sm={12} xs={12}>
                            <img src="./assets/our-team/chhinghor-circle.png" style={{height:'40vh',float:"right"}} alt="Feed img"/>
                        </Col>
                    </Row>
                    <Row className="mb-5">
                        <Col lg={4} md={4} sm={12} xs={12}>
                            <img src="./assets/our-team/sengtit.png" style={{height:'40vh'}} alt="Feed img" />
                        </Col>
                        <Col lg={8} md={8} sm={12} xs={12}>
                            <h3 className=" mb-2" style={{color:'#ffb000'}}>Tray Sengtit</h3> 
                            <b className="mb-3">DEVELOPER</b>
                            <div className=" mt-3 cus-border-button-team"></div>
                            <div className="social-media mt-3">
                                <img className="px-2" src="./assets/freelancer_pro/fb.png" alt="facebook" />
                                <img className="px-2" src="./assets/freelancer_pro/linkin.png" alt="linkIn" /> 
                                <img className="px-2" src="./assets/freelancer_pro/telegram.png" alt="telegram" /> 
                                <img className="px-2" src="./assets/freelancer_pro/twiter.png" alt="twitter" /> 
                            </div>
                            <p className="mt-4">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla finibus, orci in facilisis efficitur, tellus sem malesuada nisi, eget laoreet sapien mauris quis augueLorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla finibus, orci in facilisis efficitur, tellus sem malesuada nisi, eget laoreet sapien mauris quis augue 
                            </p>
                            <hr></hr>
                        </Col>
                    </Row>
                    <Row className="mb-5">
                        <Col lg={8} md={8} sm={12} xs={12}>
                            <div>
                                <h3 className=" mb-2" style={{color:'#ffb000'}}>Chiv Kimchhor</h3> 
                                <b className="mb-3">DEVELOPER</b>
                                <div className=" mt-3 cus-border-button-team">
                            </div>
                            <div className="social-media mt-3">
                                <img className="px-2" src="./assets/freelancer_pro/fb.png" alt="facebook" />
                                <img className="px-2" src="./assets/freelancer_pro/linkin.png" alt="linkIn" /> 
                                <img className="px-2" src="./assets/freelancer_pro/telegram.png" alt="telegram" /> 
                                <img className="px-2" src="./assets/freelancer_pro/twiter.png" alt="twitter" /> 
                            </div>
                            <p className="mt-4">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla finibus, orci in facilisis efficitur, tellus sem malesuada nisi, eget laoreet sapien mauris quis augueLorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla finibus, orci in facilisis efficitur, tellus sem malesuada nisi, eget laoreet sapien mauris quis augue 
                            </p>
                            <hr></hr>
                            </div>
                        </Col>
                        <Col lg={4} md={4} sm={12} xs={12}>
                            <img src="./assets/our-team/kimchhor.png" style={{height:'40vh',float:'right'}} alt="Feed img" />
                        </Col>
                    </Row>
                    <Row className="mb-5">
                        <Col lg={4} md={4} sm={12} xs={12}>
                            <img src="./assets/our-team/piseth.png" style={{height:'40vh'}} alt="Feed img" />
                        </Col>
                        <Col lg={8} md={8} sm={12} xs={12}>
                            <h3 className=" mb-2" style={{color:'#ffb000'}}>Thou Piseth</h3> 
                            <b className="mb-3">DEVELOPER</b>
                            <div className=" mt-3 cus-border-button-team"></div>
                            <div className="social-media mt-3">
                                <img className="px-2" src="./assets/freelancer_pro/fb.png" alt="facebook" />
                                <img className="px-2" src="./assets/freelancer_pro/linkin.png" alt="linkIn" /> 
                                <img className="px-2" src="./assets/freelancer_pro/telegram.png" alt="telegram" /> 
                                <img className="px-2" src="./assets/freelancer_pro/twiter.png" alt="twitter" /> 
                            </div>
                            <p className="mt-4">
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla finibus, orci in facilisis efficitur, tellus sem malesuada nisi, eget laoreet sapien mauris quis augueLorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla finibus, orci in facilisis efficitur, tellus sem malesuada nisi, eget laoreet sapien mauris quis augue 
                            </p>
                            <hr></hr>
                        </Col>
                    </Row>
                    <Row className="mb-5">
                        <Col lg={8} md={8} sm={12} xs={12} >
                            <div>
                                <h3 className=" mb-2" style={{color:'#ffb000'}}>Kung Porleas</h3> 
                                <b className="mb-3">DEVELOPER</b>
                                <div className=" mt-3 cus-border-button-team">
                            </div>
                            <div className="social-media mt-3">
                                <img className="px-2" src="./assets/freelancer_pro/fb.png" alt="facebook" />
                                <img className="px-2" src="./assets/freelancer_pro/linkin.png" alt="linkIn" /> 
                                <img className="px-2" src="./assets/freelancer_pro/telegram.png" alt="telegram" /> 
                                <img className="px-2" src="./assets/freelancer_pro/twiter.png" alt="twitter" /> 
                            </div>
                            <p className="mt-4">
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla finibus, orci in facilisis efficitur, tellus sem malesuada nisi, eget laoreet sapien mauris quis augueLorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla finibus, orci in facilisis efficitur, tellus sem malesuada nisi, eget laoreet sapien mauris quis augue 
                            </p>
                            <hr></hr>
                            </div>
                        </Col>
                        <Col lg={4} md={4} sm={12} xs={12}>
                            <img src="./assets/our-team/porlors.png" style={{height:'40vh',float:"right"}} alt="Feed img"/>
                        </Col>
                    </Row>
                </div>
            </Container>
        </div>
    )
}
