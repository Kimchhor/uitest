import React from 'react'
import { Container, Row ,Col,Button} from 'react-bootstrap'
import { EditOutlined } from '@ant-design/icons'
import BsCard from '../../components/freelancer/BsCard'
import { NavLink } from 'react-router-dom'


function Feed() {
    const a=[1,2,3];
    return (
        <div className="free-feed">
            <Container className="pt-5">
                <Row className="mb-5">
                    <Col lg={6} md={6} >
                        <h1 className="mt-5">Welcome MengHong !</h1>
                        <p>Have fun exploring clients and projects that are best suited for you,hopefully you will enjoy the great here.Be sure to reach out if there is anything that you concerns you . </p>
                    </Col>
                    <Col lg={6} md={6}>
                        <img src="./assets/freelancer_pro/feed_cover.png" alt="Feed img" width="100%" />
                    </Col>
                </Row>
                <Row>
                    <Col lg={4} md={4}>
                        <div className="sticky pb-2">
                            <h5 className="mb-4 title">My Categories </h5>
                            <div className="categories">
                                <Button variant="warning">Web & Mobile Design</Button>
                                <Button variant="warning">BackEnd Developement</Button>
                                <Button variant="warning">Game Design</Button>
                                <Button variant="warning">Logo Design</Button>
                                <Button variant="warning">Android Application Developement</Button>
                                <Button variant="secondary" className="btn-edit">
                                    <EditOutlined style={{fontSize:'15px'}}/> Edit Category
                                </Button>
                            </div>
                        </div>
                    </Col>
                    <Col lg={5} md={5}>
                        <h5 className="mb-4">My Feeds</h5>
                        <div className="my-feed">
                            {a.map((value) =><><BsCard /><br/></>)}
                        </div>
                    </Col>
                    <Col lg={3} md={3}>
                        <div className="sticky">
                            <h5 className="mb-4">My Profile</h5>
                            <div className="my-profile">
                                <div className="d-flex mb-2">
                                    <img className="me-4" src="./assets/freelancer_pro/mengHong.png" height="46px" alt=".."/>
                                    <h6>Than MengHong<br/>
                                    <span style={{fontSize:'12px',fontWeight:'400'}}>Full-Stack Developer</span></h6>
                                </div>
                                <p style={{fontSize:'13px',padding:'8px'}}>I’ve been working on Full-Stack Projectfor almost 10 years now. Feel free to have a chat for more info.</p>
                                <Button as={NavLink} to="/profile" variant="warning">View Profile</Button>
                            </div>
                        </div>
                    </Col>
                </Row>
            </Container>
        </div>
    )
}

export default Feed
