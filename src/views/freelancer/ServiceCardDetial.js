import React from 'react'
import './styles.css'
import { Container, Row  , Col , Button} from 'react-bootstrap'
import { NavLink } from 'react-router-dom'
import {StarFilled} from '@ant-design/icons'

function ServiceCardDetial() {
    return (
        <div className="post-detail">
            <Container>
                <div className="post-cover  my-4">
                    <img src="./assets/freelancer_pro/card.png" alt=".." width="100%"/>
                </div>
                <Row className="my-5">
                    <Col lg={4} md={4}>
                        <h5>Published By</h5>
                        <div className="posted-by mt-4">
                            <div className="d-flex">
                                <img className="float-end me-4" src="./assets/freelancer_pro/mengHong.png" height="68px" alt=".."/>
                                <div>
                                    <h6>Than MengHong</h6>
                                    <span style={{color:'#47444B',fontSize:'13px'}}>Full Stack Developer</span>
                                    <p><StarFilled style={{color:'#fbd98d',fontSize:'15px',position:'absolute',marginTop:'2px'}}/> <span style={{marginLeft:'20px'}}>5.0(20)</span></p>
                                </div>
                            </div>
                            <p style={{fontSize:'15px'}}>Hi, I am Menghong. I’ve been working on Web & Mobile Development for amlost 8 years now. </p>
                            <div className="socail-medai">
                                <img src="./assets/freelancer_pro/fb.png" alt="."/>
                                <img src="./assets/freelancer_pro/linkin.png" alt="."/>
                                <img src="./assets/freelancer_pro/telegram.png" alt="."/>
                                <img src="./assets/freelancer_pro/twiter.png" alt="."/>
                            </div>
                            <NavLink to='/profile'><Button variant="warning">View Profile</Button></NavLink>
                        </div>
                        <h5 className="my-4">Additional Information</h5>
                        <div className="addi-info">
                            <div className="d-flex">
                                <img className="float-end me-4" src="./assets/freelancer_pro/phone.png" height="35px" alt=".."/>
                                <p><b>Telephone</b><br/>012 123 321 / 091 212 342</p>
                            </div>
                            <div className="d-flex">
                                <img className="float-end me-4" src="./assets/freelancer_pro/mail.png" height="35px" alt=".."/>
                                <p><b>Official Email</b><br/>traysengtit32@gmail.com</p>
                            </div>
                            <div className="d-flex">
                                <img className="float-end me-4" src="./assets/freelancer_pro/location.png" height="35px" alt=".."/>
                                <p><b>Address</b><br/>Phnom Penh , Cambodia </p>
                            </div>
                        </div>
                    </Col>
                    <Col lg={8} md={8}>
                        <div className="des">
                            <p><b>I will development Backend, FrontEnd and  UI/UX Design for the medium budget website.</b></p>
                            <Button variant="warning">Web Development</Button>
                            <Button variant="warning">Backend Development</Button><br/><br/>
                            <p>Hello,I am looking for someone to design the below please:</p>
                            <ul>
                                <li>Business Cards</li>
                                <li> Letterheads</li>
                                <li>A4 & DL Envelops</li>
                                <li>Notepad </li>
                                <li>Business Cards</li>
                                <li>Letterheads</li>
                                <li>A4 & DL Envelops</li>
                                <li>Notepad</li>
                            </ul>
                            <p>
                                A ReactJS developer is responsible for designing and implementing UI components for JavaScript-based web applications and mobile applications with the use of open-source library infrastructure. These developers are a part of the entire process starting from conception to the major testing process and follow popular ReactJS workflows like Flux.
                                <br/><br/>
                                ReactJS developers are front-end developers who build modern-day UI components to improvise application performance. They leverage their knowledge about JavaScript, HTML, CSS and work closely with testers, designers, web designers, and project managers to create a robust and effective application.
                                <br/><br/>
                                Project Budget : 1999 USD <br/>
                                Duration : 2 months<br/>
                                Links: <a href=" "> https://i.pinimg.com/564x/32/c6/1d/32c61d1e68b7253f5ba0078270b8f38b.jpg</a><br/>
                            </p>
                            <p><StarFilled style={{color:'#fbd98d',fontSize:'15px',position:'absolute',marginTop:'2px'}}/> <span style={{marginLeft:'20px'}}>4.5(127)</span></p>
                        </div>
                    </Col>
                </Row>
                <Row className="review">
                    <Col lg={2} md={2}></Col>
                    <Col lg={10} md={10}>
                        <b>10,642 Reviews</b>
                        <div className="comment d-flex mt-4">
                            <img className="float-end me-4" src="./assets/freelancer_pro/kimchhor.png" height="56px" alt=".."/>
                            <div>
                                <div className="d-flex">
                                    <h6>Chiv Kimchhor</h6>
                                    <p className="ms-3">
                                        <StarFilled style={{color:'#fbd98d',fontSize:'15px',position:'absolute',marginTop:'4px'}}/> <span style={{marginLeft:'20px'}}>5</span>
                                    </p>
                                </div>
                                <p className="post-date" style={{marginTop:'-13px'}}>Posted : 14th February 2021</p>
                                <p>
                                    I’m really interested in this project . Would you mind me asking about the architecture of the application or the structure of the programming , is it write whatever that work or there is a certain set of rules to follow ?
                                </p>
                            </div>
                        </div>
                        <div className="reply d-flex mt-4 ms-5">
                            <img className="float-end me-4" src="./assets/freelancer_pro/mengHong.png" height="56px" alt=".."/>
                            <div>
                                <h6>Than MengHong</h6>
                                <p className="post-date">Posted : 14th February 2021</p>
                                <p>
                                    I’m really interested in this project . Would you mind me asking about the architecture of the application or the structure of the programming , is it write whatever that work or there is a certain set of rules to follow ?
                                </p>
                            </div>
                        </div>
                    </Col>
                </Row>
            </Container>
        </div>
    )
}

export default ServiceCardDetial
