import React from 'react'
import { BrowserRouter as Router , Route , Switch} from 'react-router-dom'
import ProfileDetail from './ProfileDetail'
import ServiceCardDetial from './ServiceCardDetial'
import NavFree from '../../components/freelancer/NavBar'
import ServiceCardPost from './ServiceCardPost'
import EditeProfile from './EditeProfile'
import Feed from './Feed'
import BsCardDetail from './BsCardDetail'
import BsProfileDetail from './BsProfileDetail'
import Footer from '../../components/Footer'
import AboutUs from '../AboutUs'

function Freelancer() {
    return (
        <>
            <Router>
                <NavFree />
                    <Switch>
                        <Route path="/bs_profile">
                            <BsProfileDetail />
                        </Route>
                        <Route path="/bs_card_detail">
                            <BsCardDetail />
                        </Route>
                        <Route path="/profile">
                            <ProfileDetail />
                        </Route>
                        <Route path="/detail">
                            <ServiceCardDetial />
                        </Route>
                        <Route path="/post_service">
                            <ServiceCardPost />
                        </Route>
                        <Route path="/profile_edit">
                            <EditeProfile/>
                        </Route>
                        <Route path="/aboutus">
                            <AboutUs />
                        </Route>
                        <Route path="/">
                            <Feed />
                        </Route>
                    </Switch>
                <Footer/>
            </Router>
        </>
    )
}

export default Freelancer
