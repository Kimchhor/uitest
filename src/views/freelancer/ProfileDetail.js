import React from 'react'
import './styles.css'
import { Tabs } from 'antd'
import { Col, Container, Row } from 'react-bootstrap'
import Card from '../../components/freelancer/Card'
import { NavLink } from 'react-router-dom'

const { TabPane } = Tabs;

function ProfileDetail() {
    const a = [1,2,3,4,5];
    return (
        <div className="profile-detail">
            <div className="cover">
                <img src="./assets/freelancer_pro/cover.png" alt="no cover show" width="100%"/>
            </div>
            <Container>
                <Row>
                    <Col lg={2} md={2}>
                        <div className="profile">
                            <img src="./assets/freelancer_pro/menghong.png" alt=".." />
                        </div>
                    </Col>
                    <Col lg={8} md={8} className="mt-3">
                        <h4>Than MengHong</h4>
                        <p style={{color:'#47444B',fontSize:'14px'}}>Full Stack Developer</p>
                        <p>
                        Hi, I’m Menghong . I’ve been working on Web & Mobile Development for almost 8 years now.Glad that you made it here. Please feel free to consult anything. Appreciate everything....
                        </p>
                    </Col>
                    <Col lg={2} md={2} className="mt-3">
                        <NavLink to="profile_edit">
                            <img className="float-end" src="./assets/freelancer_pro/setting.png" alt=".."/>
                        </NavLink>
                    </Col>
                </Row>
                {/* ............... */}
                <Row className="mt-4">
                    <Col lg={4} md={4}>
                        <h5 className="mb-3">Contact Information</h5>
                        <div className="contact-info">
                            <div className="d-flex">
                                <img className="float-end me-4" src="./assets/freelancer_pro/phone.png" height="35px" alt=".."/>
                                <p><b>Telephone</b><br/>012 123 321 / 091 212 342</p>
                            </div>
                            <div className="d-flex">
                                <img className="float-end me-4" src="./assets/freelancer_pro/mail.png" height="35px" alt=".."/>
                                <p><b>Official Email</b><br/>traysengtit32@gmail.com</p>
                            </div>
                            <div className="d-flex">
                                <img className="float-end me-4" src="./assets/freelancer_pro/location.png" height="35px" alt=".."/>
                                <p><b>Address</b><br/>Phnom Penh , Cambodia </p>
                            </div>
                            <div className="d-flex">
                                <img className="float-end me-4" src="./assets/freelancer_pro/nation.png" height="35px" alt=".."/>
                                <p><b>Nationality</b><br/>Khmer</p>
                            </div>
                            <div className="socail-medai">
                                <img src="./assets/freelancer_pro/fb.png" alt="."/>
                                <img src="./assets/freelancer_pro/linkin.png" alt="."/>
                                <img src="./assets/freelancer_pro/telegram.png" alt="."/>
                                <img src="./assets/freelancer_pro/twiter.png" alt="."/>
                            </div>
                        </div>
                        {/* ..........Experiences & Achivement........... */}
                        <Tabs type="card" className="mt-4 ex-ac">
                            <TabPane tab="Experiences" key="1">
                                <div className="experience">
                                    <ul>
                                        <li>Backend Developer at Smart axiata, Phnom penh</li>
                                        <li>FullStack Dev at Techo Startup, Phnom Penh</li>
                                        <li>Network Engineer at Metfone, Phnom Penh</li>
                                        <li>Graphic Designer at Mobitel, Phnom Penh</li>
                                        <li>Former staff at Google</li>
                                    </ul>
                                </div>
                            </TabPane>
                            <TabPane tab="Achivement" key="2">
                                <div className="achivement">
                                    <ul>
                                        <li>Backend Developer at Smart axiata, Phnom penh</li>
                                        <li>FullStack Dev at Techo Startup, Phnom Penh</li>
                                        <li>Network Engineer at Metfone, Phnom Penh</li>
                                        <li>Graphic Designer at Mobitel, Phnom Penh</li>
                                        <li>Former staff at Google</li>
                                    </ul>
                                </div>
                            </TabPane>
                        </Tabs>
                        {/* -------------Educational Background----------------- */}
                        <h5 className="mb-3 mt-4">Educational Background</h5>
                            <div className="education">
                                <ul>
                                    <li>Backend Developer at Smart axiata, Phnom penh</li>
                                    <li>FullStack Dev at Techo Startup, Phnom Penh</li>
                                    <li>Network Engineer at Metfone, Phnom Penh</li>
                                    <li>Graphic Designer at Mobitel, Phnom Penh</li>
                                    <li>Former staff at Google</li>
                                </ul>
                            </div>
                    </Col>
                    <Col lg={8} md={8}>
                        <h5 className="mb-3">Service Card Dashboard</h5>
                        <Tabs type="card">
                            <TabPane tab="Service Cards" key="1">
                                <p className="mt-3">Number of service card is : 120 projects </p>
                            {a.map((value) => {                                                              
                                    return  (
                                        <><Card/><br/></>
                                    )
                                }
                            )}
                            </TabPane>
                            <TabPane tab="Pending" key="2">
                                <p className="mt-3">Number of service card is : 120 projects </p>
                                {a.map((value) => {                                                              
                                        return  (
                                            <div className="pending">
                                                <Card/><br/>
                                            </div>
                                        )
                                    }
                                )}
                            </TabPane>
                            <TabPane tab="Draft" key="3">
                                <p className="mt-3">Number of service card is : 120 projects </p>
                                {a.map((value) => {                                                              
                                        return  (
                                            <div className="draft">
                                                <Card/><br/>
                                            </div>
                                        )
                                    }
                                )}
                            </TabPane>
                        </Tabs>
                    </Col>
                </Row>
            </Container>
        </div>
    )
}

export default ProfileDetail
