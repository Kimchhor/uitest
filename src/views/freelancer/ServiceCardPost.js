import React from 'react'
import { Col, Container, Row ,Button,Form} from 'react-bootstrap'
import { Input,Select } from 'antd';

const { TextArea } = Input;
const { Option } = Select;

function ServiceCardPost() {
    return (
        <div className="post-service my-5">
            <Container>
                <h3>Service Card Post</h3>
                <Row>
                    <Col lg={7} md={7}>
                        <div className="mt-5">
                            <h5>Service Card Title </h5>
                            <TextArea placeholder="Enter your card title" allowClear />
                        </div>
                        <div className="mt-4">
                            <h5>Categories</h5>
                            <Select defaultValue="Select related categories" className="select-after">
                                <Option value="web">Web & Mobile Design</Option>
                                <Option value="backend">BackEnd Developement</Option>
                                <Option value="gamedesign">Game Design</Option>
                                <Option value="logo">Logo Design</Option>
                            </Select>
                        </div>
                        <div className="mt-4">
                            <h5>Card Description</h5>
                            <TextArea placeholder="Enter your service card description...." style={{height:'200px'}} allowClear />
                        </div>
                        <Button className="cancel" variant="warning">Cancel</Button>
                        <div className="float-end">
                            <Button className="me-4 draft" variant="warning">Draft</Button>
                            <Button variant="warning">Post</Button>
                        </div>
                    </Col>
                    <Col lg={5} md={5}>
                        <div className="choose-img">
                            <div className="seleted-img">
                                <img src="/assets/freelancer_pro/defual_img.png" alt="..."/>
                            </div>
                            <Form.Group controlId="formFileMultiple">
                                <Form.Label className="mt-4">Choose Image</Form.Label><br/>
                                <Form.Control type="file" multiple />
                            </Form.Group>
                        </div>
                    </Col>
                </Row>
            </Container>
        </div>
    )
}

export default ServiceCardPost
