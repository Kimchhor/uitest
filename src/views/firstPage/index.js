import React from 'react'
import { BrowserRouter as Router, Route , Switch } from 'react-router-dom'
import Sahakkalogin from '../../auth/login/Sahakkalogin'
import NavMenu from '../../components/firstPage/NavHome'
import Footer from '../../components/Footer'
import Home from './Home'
import CreatAnAccount from '../../auth/signup/CreatAnAccount'
import PersonalInfo from '../../auth/signup/businessOwner/PersonalInfo'
import ContactInfo from '../../auth/signup/businessOwner/ContactInfo'
import CompanyInfo from '../../auth/signup/businessOwner/CompanyInfo'
import FreePersonalInfo from '../../auth/signup/freelancer/FreePersonalInfo'
import FreeContactInfo from '../../auth/signup/freelancer/FreeContactInfo'
import ExperienceExpertise from '../../auth/signup/freelancer/ExperienceExpertise'
import FreeAchievement from '../../auth/signup/freelancer/FreeAchievement'
import AboutUs from '../AboutUs'
import LearnMoreFree from './LearnMoreFree'
import LearnMoreBus from './LearnMoreBus'

function FirstPage() {
    return (
        <div>
            <Router>
                <NavMenu/>
                <Switch>
                    <Route path="/login">
                        <Sahakkalogin />
                    </Route>
                    <Route path="/signup">
                        <CreatAnAccount />
                    </Route>
                    <Route path="/signup_fre_personalinfo">
                        <FreePersonalInfo />
                    </Route>
                    <Route path="/signup_fre_contactinfo">
                        <FreeContactInfo />
                    </Route>
                    <Route path="/signup_experience">
                        <ExperienceExpertise />
                    </Route>
                    <Route path="/signup_achievements">
                        <FreeAchievement />
                    </Route>
                    <Route path="/signup_bu_personalinfo">
                        <PersonalInfo />
                    </Route>
                    <Route path="/signup_bu_contactinfo">
                        <ContactInfo />
                    </Route>
                    <Route path="/signup_bu_companyinfo">
                        <CompanyInfo />
                    </Route>
                    <Route path="/aboutus">
                        <AboutUs />
                    </Route>
                    <Route path="/learnmore_free">
                        <LearnMoreFree/>
                    </Route>
                    <Route path="/learnmore_bus">
                        <LearnMoreBus />
                    </Route>
                    <Route path="/">
                        <Home/>
                    </Route>
                </Switch>
                <Footer />
            </Router>
        </div>
    )
}

export default FirstPage
