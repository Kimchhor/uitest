import React from 'react'
import './styles.css'
import { Container, Row, Col, Button } from 'react-bootstrap'
import OwlCarousel from "react-owl-carousel";
import "owl.carousel/dist/assets/owl.carousel.css";
import "owl.carousel/dist/assets/owl.theme.default.css";
import { NavLink } from 'react-router-dom';

export default function Home() {
    return (
        <div className="first-page">
            <Container className="py-5">
                <Row className="mb-5">
                    <Col lg={6} md={6}>
                        <h1 className="mt-5">SAHAKKA</h1>
                        <p>A bussiness solution for both clients and freelancers aiming to provide best freelance services as a win-win stategy between  the two consumers.Signing up to post your projects and grow your bisiness or to earn from your expertise. </p>
                        <div className="float-start">
                            <Button className="me-4 draft" variant="warning">Freelancer</Button>
                            <Button variant="warning">Client</Button>
                        </div>
                    </Col>
                    <Col lg={6} md={6}>
                        <img src="./assets/popular/banner.png" alt="Feed img" width="100%" />
                    </Col>
                </Row>
                <Row>
                    {/* Categories Owl carousel */}
                    <h3 className="mb-5">Explore the popular categories</h3>
                    <div className="categories">
                        <NavLink to="/">
                            <OwlCarousel
                                className="owl-theme"
                                loop margin={10}
                                nav={true}
                                dots={false}
                                items={5}
                            >

                                <div class="item">
                                    <img src="./assets/business_img/cat1.png" alt="no img show" />
                                </div>
                                <div class="item">
                                    <img src="./assets/business_img/cat2.png" alt="no img show" />
                                </div>
                                <div class="item">
                                    <img src="./assets/business_img/cat3.png" alt="no img show" />
                                </div>
                                <div class="item">
                                    <img src="./assets/business_img/cat4.png" alt="no img show" />
                                </div>
                                <div class="item">
                                    <img src="./assets/business_img/cat5.png" alt="no img show" />
                                </div>
                                <div class="item">
                                    <img src="./assets/business_img/cat2.png" alt="no img show" />
                                </div>
                                <div class="item">
                                    <img src="./assets/business_img/cat3.png" alt="no img show" />
                                </div>
                            </OwlCarousel>
                        </NavLink>
                    </div>
                </Row>
                <Row className="my-5">
                    <Col lg={6} md={6}>
                        <img src="./assets/popular/banner-second.png" alt="Feed img" width="100%" />
                    </Col>
                    <Col lg={6} md={6}>
                        <h1 className="mt-5">For Freelancers</h1>
                        <p>One of the biggest advantages of working independently as a freelancer is having freedom in choosing clients and projects without any restrictions. Freelancing makes it very simple for a person to choose the type of work based on what interests and benefits them best.  Having a immense flexibility of locations which means we can work anywhere amd the possibility are endless. </p>
                        <div className="float-start">
                            <NavLink to="/learnmore_free">
                                <Button className="me-4 draft" variant="warning">Learning More</Button>
                            </NavLink>
                            <Button variant="warning">Start Freelancing</Button>
                        </div>
                    </Col>
                </Row>
                {/* ----------------- */}
                <h3 className="text-center mb-5">Benefits Of <span style={{ color: '#ffb000' }}>Freelancing</span></h3>
                <Row>
                    <Col lg={4} md={4} className="text-center">
                        <img src="./assets/benefit/benefit1.png" width="50%" alt="no img show" />
                        <h6 className="mt-2">Opportunities To Earn More</h6>
                    </Col>
                    <Col lg={4} md={4} className="text-center">
                        <img src="./assets/benefit/benefit2.png" width="50%" alt="no img show" />
                        <h6 className="mt-2">Choose The Right Client And Project <br /> That Are Best For You </h6>
                    </Col>
                    <Col lg={4} md={4} className="text-center">
                        <img src="./assets/benefit/benefit3.png" width="50%" alt="no img show" />
                        <h6 className="mt-2">Immense Flexibility & Multi-Facted <br /> Exposuee</h6>
                    </Col>
                </Row>
                {/* ................ */}

                <Row className="my-5">
                    <Col lg={6} md={6}>
                        <h1 className="mt-5">For Clients</h1>
                        <p>Today’s workforce is evolving at an unbelievable pace. While yesterday’s professionals used to be eight-hour workday, modern workers are now has more flexible work arrangement. Working with freelancers offers lots of growth potential for business since it is cost-effective and convinient. </p>
                        <div className="float-start">
                            <NavLink to="/learnmore_bus">
                                <Button className="me-4 draft" variant="warning">Learn More</Button>
                            </NavLink>
                            <Button variant="warning">Start Hiring</Button>
                        </div>
                    </Col>
                    <Col lg={6} md={6}>
                        <img src="./assets/popular/banner-third.png" alt="Feed img" width="100%" />
                    </Col>
                </Row>
                {/* ----------------- */}
                <h3 className="text-center mb-5">Benefits <span style={{ color: '#ffb000' }}>Hiring</span> Freelancer</h3>
                <Row>
                    <Col lg={4} md={4} className="text-center">
                        <img src="./assets/benefit/benefit1.png" width="50%" alt="no img show" />
                        <h6 className="mt-2">Flexibility & Top-Tier Selection</h6>
                    </Col>
                    <Col lg={4} md={4} className="text-center">
                        <img src="./assets/benefit/benefit2.png" width="50%" alt="no img show" />
                        <h6 className="mt-2">Cost-Effective Outcome</h6>
                    </Col>
                    <Col lg={4} md={4} className="text-center">
                        <img src="./assets/benefit/benefit3.png" width="50%" alt="no img show" />
                        <h6 className="mt-2">Better Work Quality</h6>
                    </Col>
                </Row>
            </Container>
        </div>
    )
}
