import React from 'react'
import { Container, Row ,Col,Button} from 'react-bootstrap'
import OwlCarousel from "react-owl-carousel";
import "owl.carousel/dist/assets/owl.carousel.css";
import "owl.carousel/dist/assets/owl.theme.default.css";

export default function LearnMoreBus() {
    return (
        <div className="first-page" style={{marginBottom:'6em'}}>
            <Container className="mb-5">
                <Row className="mt-5">
                    <Col lg={6} md={6} xs={12}>
                        <h1 className="mb-3" style={{marginTop:'3em'}}>Hiring on <span style={{color:'#ffb000'}}>SAHAKKA</span></h1>
                        <p>A bussiness solution for both clients and freelancers aiming to provide best freelance services as a win-win stategy between  the two consumers.Signing up to post your projects and grow your bisiness or to earn from your expertise. </p>
                        <div className="float-start">
                            <Button className="me-4 draft" variant="warning">Start working</Button>
                        </div>
                    </Col>
                    <Col lg={6} md={6} xs={12}>
                        <img src="./assets/freelancer_pro/banner-human.png" alt="Feed img" width="100%" />
                    </Col>
                </Row>
                <h2 className="text-center my-5">Start Hiring in <span style={{color:'#ffb000'}}>3 Simple Steps</span></h2>
                <Row>
                    <Col lg={4} md={4} className="text-center">
                        <img src="./assets/freelancer_pro/sample2.png" width="30%" alt="no img show" />
                        <h6 className=" text-center mt-2">Create your profile</h6>
                        <p>create your own professional profile, so that you can showcase your skills and experties </p>
                    </Col>
                    <Col lg={4} md={4} className="text-center">
                        <img src="./assets/freelancer_pro/sample1.png" width="30%" alt="no img show" />
                        <h6 className=" text-center mt-2">Explore Projects</h6>
                        <p>Find the clients and the projects that are best suited for you.This is what we call freedom in the freelancing industry</p>
                    </Col>
                    <Col lg={4} md={4} className="text-center">
                        <img src="./assets/freelancer_pro/sample4.png" width="30%" alt="no img show" />
                        <h6 className="text-center mt-2">Work & Earn</h6>
                        <p>Start working on your desired projects and get paid from it.The more you get the work done that more you earn</p>
                    </Col>
                </Row>
                <h3 className="my-5">Explore the popular projects</h3>
                <OwlCarousel 
                    className="owl-theme" 
                    loop 
                    margin={10} 
                    nav
                    autoplay ={true} 
                    items={5} 
                    autoplayTimeout={2000}
                    dots={false}
                >
                    <div class="item">
                        <img src="./assets/popular/p1.jpg" width="60" height="300"/>
                    </div>
                    <div class="item">
                        <img src="./assets/popular/p2.jpg" width="60" height="300"/>
                    </div>
                    <div class="item">
                        <img src="./assets/popular/p3.jpg" width="60" height="300"/>
                    </div>
                    <div class="item">
                        <img src="./assets/popular/p4.jpg" width="60" height="300" />
                    </div>
                    <div class="item">
                        <img src="./assets/popular/p5.jpg" width="60" height="300"/>
                    </div>
                    <div class="item">
                        <img src="./assets/popular/p4.jpg" width="60" height="300" />
                    </div>
                </OwlCarousel>
            </Container>
        </div>
    )
}
