import React from 'react'
import './styles.css'
import FreeCard from '../../components/BusinessOwner/FreeCard'
import { Tabs } from 'antd'
import { Col, Container, Row , Button } from 'react-bootstrap'

const { TabPane } = Tabs;

function FreeProfileDetail() {
    const a = [1,2,3,4,5,6,7,8,9];
    return (
        <div className="profile-detail">
            <div className="cover">
                <img src="./assets/freelancer_pro/cover.png" alt="no cover show" width="100%"/>
            </div>
            <Container>
                <Row>
                    <Col lg={2} md={2}>
                        <div className="profile">
                            <img src="./assets/freelancer_pro/menghong.png" alt=".." />
                        </div>
                    </Col>
                    <Col lg={8} md={8} className="mt-3">
                        <h4>Than MengHong</h4>
                        <p style={{color:'#47444B',fontSize:'14px'}}>Full Stack Developer</p>
                        <p>
                        Hi, I’m Menghong . I’ve been working on Web & Mobile Development for almost 8 years now.Glad that you made it here. Please feel free to consult anything. Appreciate everything....
                        </p>
                    </Col>
                    <Col lg={2} md={2} className="mt-3">
                        <Button variant="warning" className="btn-contact-me">Contact Me</Button>
                    </Col>
                </Row>
                {/* ............... */}
                <Row className="mt-4">
                    <Col lg={4} md={4}>
                        <h5 className="mb-3">Contact Information</h5>
                        <div className="contact-info">
                            <div className="d-flex">
                                <img className="float-end me-4" src="./assets/freelancer_pro/phone.png" height="35px" alt=".."/>
                                <p><b>Telephone</b><br/>012 123 321 / 091 212 342</p>
                            </div>
                            <div className="d-flex">
                                <img className="float-end me-4" src="./assets/freelancer_pro/mail.png" height="35px" alt=".."/>
                                <p><b>Official Email</b><br/>traysengtit32@gmail.com</p>
                            </div>
                            <div className="d-flex">
                                <img className="float-end me-4" src="./assets/freelancer_pro/location.png" height="35px" alt=".."/>
                                <p><b>Address</b><br/>Phnom Penh , Cambodia </p>
                            </div>
                            <div className="d-flex">
                                <img className="float-end me-4" src="./assets/freelancer_pro/nation.png" height="35px" alt=".."/>
                                <p><b>Nationality</b><br/>Khmer</p>
                            </div>
                            <div className="socail-medai">
                                <img src="./assets/freelancer_pro/fb.png" alt="."/>
                                <img src="./assets/freelancer_pro/linkin.png" alt="."/>
                                <img src="./assets/freelancer_pro/telegram.png" alt="."/>
                                <img src="./assets/freelancer_pro/twiter.png" alt="."/>
                            </div>
                        </div>
                        {/* ..........Experiences & Achivement........... */}
                        <Tabs type="card" className="mt-4 ex-ac">
                            <TabPane tab="Experiences" key="1">
                                <div className="experience">
                                    <ul>
                                        <li>Backend Developer at Smart axiata, Phnom penh</li>
                                        <li>FullStack Dev at Techo Startup, Phnom Penh</li>
                                        <li>Network Engineer at Metfone, Phnom Penh</li>
                                        <li>Graphic Designer at Mobitel, Phnom Penh</li>
                                        <li>Former staff at Google</li>
                                    </ul>
                                </div>
                            </TabPane>
                            <TabPane tab="Achivement" key="2">
                                <div className="achivement">
                                    <ul>
                                        <li>Backend Developer at Smart axiata, Phnom penh</li>
                                        <li>FullStack Dev at Techo Startup, Phnom Penh</li>
                                        <li>Network Engineer at Metfone, Phnom Penh</li>
                                        <li>Graphic Designer at Mobitel, Phnom Penh</li>
                                        <li>Former staff at Google</li>
                                    </ul>
                                </div>
                            </TabPane>
                        </Tabs>
                        {/* -------------Educational Background----------------- */}
                        <h5 className="mb-3 mt-4">Educational Background</h5>
                            <div className="education">
                                <ul>
                                    <li>Backend Developer at Smart axiata, Phnom penh</li>
                                    <li>FullStack Dev at Techo Startup, Phnom Penh</li>
                                    <li>Network Engineer at Metfone, Phnom Penh</li>
                                    <li>Graphic Designer at Mobitel, Phnom Penh</li>
                                    <li>Former staff at Google</li>
                                </ul>
                            </div>
                    </Col>
                    <Col lg={8} md={8}>
                        <Row style={{marginTop:'2.7em',marginBottom:'2.7em'}}>
                            {a.map((value) => {                                                              
                                    return  (
                                        <Col lg={4} md={4}>
                                            <FreeCard />
                                        <br/></Col>
                                    )
                                }
                            )}
                        </Row>
                        <b>10,642 Reviews</b>
                        <div className="comment d-flex mt-4">
                            <img className="float-end me-4" src="./assets/freelancer_pro/user-no-pro.png" height="56px" alt=".."/>
                            <div>
                                <h6>Kay Keo</h6>
                                <p className="post-date">Posted : 14th February 2021</p>
                                <p>
                                    I’m really interested in this project . Would you mind me asking about the architecture of the application or the structure of the programming , is it write whatever that work or there is a certain set of rules to follow ?
                                </p>
                            </div>
                        </div>
                        <div className="comment d-flex mt-4">
                            <img className="float-end me-4" src="./assets/freelancer_pro/kimchhor.png" height="56px" alt=".."/>
                            <div>
                                <h6>Chiv Kimchho</h6>
                                <p className="post-date">Posted : 14th February 2021</p>
                                <p>
                                    I’m really interested in this project . Would you mind me asking about the architecture of the application or the structure of the programming , is it write whatever that work or there is a certain set of rules to follow ?
                                </p>
                            </div>
                        </div>
                    </Col>
                </Row>
            </Container>
        </div>
    )
}

export default FreeProfileDetail
