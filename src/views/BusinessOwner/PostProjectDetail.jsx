import React from 'react'
import './styles.css'
import { Container, Row  , Col , Button} from 'react-bootstrap'
import { NavLink } from 'react-router-dom'
import { LikeOutlined , CommentOutlined} from '@ant-design/icons'
import CopComment from '../../components/Comment'

function PostProjectDetail() {
    return (
        <div className="post-detail bs-card-detail">
            <Container>
                <div className="post-cover  my-4">
                    <img src="./assets/business_img/pro-img.png" alt=".." width="100%"/>
                </div>
                <Row className="my-5">
                    <Col lg={4} md={4}>
                        <h5>Published By</h5>
                        <div className="posted-by mt-4">
                            <div className="d-flex mb-2">
                                <img className="me-4" src="./assets/business_img/keo.png" height="46px" alt=".."/>
                                <h6>Kay Keo<br/>
                                <NavLink to="/profile_detail" style={{fontSize:'12px',fontWeight:'400',color:'#ffb000',textDecoration:'none'}}><span>View Profile</span></NavLink></h6>
                            </div>
                            <p style={{marginLeft:'4em',padding:'8px'}}>Hello there . I’m Keo . I’m currently working at an IITin the position of a Director.   </p>
                            <div className="socail-medai">
                                <img src="./assets/freelancer_pro/fb.png" alt="."/>
                                <img src="./assets/freelancer_pro/linkin.png" alt="."/>
                                <img src="./assets/freelancer_pro/telegram.png" alt="."/>
                                <img src="./assets/freelancer_pro/twiter.png" alt="."/>
                            </div>
                        </div>
                        <h5 className="my-4">Additional Information</h5>
                        <div className="addi-info">
                            <div className="d-flex">
                                <img className="float-end me-4" src="./assets/freelancer_pro/duration.png" alt=".."/>
                                <p><b>Project Duration</b><br/>1 to 3 months </p>
                            </div>
                            <div className="d-flex">
                                <img className="float-end me-4" src="./assets/freelancer_pro/public.png" alt=".."/>
                                <p><b>Published Date</b><br/>14 Feburay 2022</p>
                            </div>
                            <div className="d-flex">
                                <img className="float-end me-4" src="./assets/freelancer_pro/deadline.png" alt=".."/>
                                <p><b>Deadline</b><br/>15 Feburay 2022 </p>
                            </div>
                            <div className="d-flex">
                                <img className="float-end me-4" src="./assets/freelancer_pro/location.png" alt=".."/>
                                <p><b>Location</b><br/>Phnom Penh , Cambodia</p>
                            </div>
                        </div>
                    </Col>
                    <Col lg={8} md={8}>
                        <div className="des">
                            <p><b>I will development Backend, FrontEnd and  UI/UX Design for the medium budget website.</b></p>
                            <Button variant="warning">Web Development</Button>
                            <Button variant="warning">Backend Development</Button><br/><br/>
                            <p>Hello,I am looking for someone to design the below please:</p>
                            <ul>
                                <li>Business Cards</li>
                                <li> Letterheads</li>
                                <li>A4 & DL Envelops</li>
                                <li>Notepad </li>
                                <li>Business Cards</li>
                                <li>Letterheads</li>
                                <li>A4 & DL Envelops</li>
                                <li>Notepad</li>
                            </ul>
                            <p>
                                A ReactJS developer is responsible for designing and implementing UI components for JavaScript-based web applications and mobile applications with the use of open-source library infrastructure. These developers are a part of the entire process starting from conception to the major testing process and follow popular ReactJS workflows like Flux.
                                <br/><br/>
                                ReactJS developers are front-end developers who build modern-day UI components to improvise application performance. They leverage their knowledge about JavaScript, HTML, CSS and work closely with testers, designers, web designers, and project managers to create a robust and effective application.
                                <br/><br/>
                                Project Budget : 1999 USD <br/>
                                Duration : 2 months<br/>
                                Links: <a href=" "> https://i.pinimg.com/564x/32/c6/1d/32c61d1e68b7253f5ba0078270b8f38b.jpg</a><br/>
                            </p>
                            <div className="d-flex mt-4">
                                <p className="me-5"><LikeOutlined style={{fontSize:'25px',verticalAlign:'text-bottom'}}/> 12 Likes</p>
                                <p><CommentOutlined style={{fontSize:'25px',verticalAlign:'text-bottom'}}/> 12 Comments</p>
                            </div>
                        </div>
                    </Col>
                </Row>
                <Row className="review">
                    <Col lg={2} md={2}></Col>
                    <Col lg={10} md={10}>
                        <b>10,642 Reviews</b>
                        <CopComment/>
                        <div className="comment d-flex mt-4">
                            <img className="float-end me-4" src="./assets/freelancer_pro/user-no-pro.png" height="56px" alt=".."/>
                            <div>
                                <h6>Kay Keo</h6>
                                <p className="post-date">Posted : 14th February 2021</p>
                                <p>
                                    I’m really interested in this project . Would you mind me asking about the architecture of the application or the structure of the programming , is it write whatever that work or there is a certain set of rules to follow ?
                                </p>
                            </div>
                        </div>
                        <div className="reply d-flex mt-4 ms-5">
                        <img className="float-end me-4" src="./assets/freelancer_pro/kimchhor.png" height="56px" alt=".."/>
                            <div>
                                <h6>Chiv Kimchhor</h6>
                                <p className="post-date">Posted : 14th February 2021</p>
                                <p>
                                    I’m really interested in this project . Would you mind me asking about the architecture of the application or the structure of the programming , is it write whatever that work or there is a certain set of rules to follow ?
                                </p>
                            </div>
                        </div>
                    </Col>
                </Row>
            </Container>
        </div>
    )
}

export default PostProjectDetail
