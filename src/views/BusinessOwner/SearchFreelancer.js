import React from 'react'
import './styles.css'
import { Container , Row , Col } from 'react-bootstrap'
import FreeCard from '../../components/BusinessOwner/FreeCard'

function SearchFreelancer() {
    const a=[1,2,3,4,5,6,7,8,9,10,11,12]
    return (
        <div className="search-by-category">
            <Container>
                <Row className="my-3">
                    <Col lg={6} md={6}>
                        <h2 className="category-title">BackEnd Development</h2>
                        <p className="my-4">BackEnd Development refer the server development which involves server , an application ,and database.</p>
                    </Col>
                    <Col lg={6} md={6}>
                        <img src="./assets/business_img/search-cover.png" width="100%" alt="cover" />
                    </Col>
                </Row>
                <h5 className="mb-4">Here are the result : 121  services available </h5>
                <Row className="services-available">
                    {a.map((value)=>{
                        return(
                            <Col lg={3} md={3}>
                                <FreeCard/>
                            </Col>
                        )
                    })}
                </Row>
            </Container>
        </div>
    )
}

export default SearchFreelancer
