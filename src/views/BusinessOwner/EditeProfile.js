import React from 'react'
import { Col, Container, Row , Button} from 'react-bootstrap';
import { Form,Tabs, Input} from 'antd';
import { 
    UserOutlined, FlagOutlined , GlobalOutlined , PushpinOutlined
    , PhoneOutlined , MailOutlined ,LockOutlined
} from '@ant-design/icons'
import UploadImg from '../../components/UploadImg';

const { TabPane } = Tabs;
const { TextArea } = Input;

function EditeProfile() {
    return (
        <div className="edit-pro mt-5">
            <Container>
                <Tabs type="card">
                    <TabPane tab="Personal Information" key="1">
                        <Row className="mt-4">
                            <Col lg={5} md={5}>
                                <img src="./assets/freelancer_pro/cover-edit.png" width="100%" alt=".."/>
                            </Col>
                            <Col lg={7} md={7}>
                                <h5 className="mb-4">Personal Information</h5>
                                <Form.Item label="Full Name">
                                    <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Full Name"/>
                                </Form.Item>
                                <div className="d-flex">
                                    <Form.Item label="Nationality">
                                        <Input prefix={<FlagOutlined className="site-form-item-icon" />} placeholder="Nation"/>
                                    </Form.Item>
                                    <Form.Item label="Gender" className="ms-2">
                                        <Input prefix={<UserOutlined className="site-form-item-icon" />} placeholder="Gender"/>
                                    </Form.Item>
                                </div>
                                <Form.Item label="Address" >
                                    <Input prefix={<PushpinOutlined className="site-form-item-icon" />} placeholder="Address"/>
                                </Form.Item>
                                <Form.Item label="Bio" >
                                    <TextArea placeholder="Bio" style={{height:'100px'}}/>
                                </Form.Item>
                                <div className="my-5">
                                    <Button className="cancel" variant="warning">Cancel</Button>
                                    <Button variant="warning" className="float-end">Update</Button>
                                </div>
                            </Col>
                        </Row>
                    </TabPane>
                    <TabPane tab="Contact Information" key="2">
                        <Row className="mt-4">
                            <Col lg={5} md={5}>
                                <img src="./assets/freelancer_pro/cover-edit.png" width="100%" alt=".."/>
                            </Col>
                            <Col lg={7} md={7}>
                                <h5 className="mb-4">Contact Information</h5>
                                <Form.Item label="Telephone">
                                    <Input prefix={<PhoneOutlined className="site-form-item-icon" />} placeholder="Telephone"/>
                                </Form.Item>
                                <Form.Item label="Official Email">
                                    <Input prefix={<MailOutlined className="site-form-item-icon" />} placeholder="Official Email"/>
                                </Form.Item>
                                <Form.Item label="Website">
                                    <Input prefix={<GlobalOutlined className="site-form-item-icon" />} placeholder="Website"/>
                                </Form.Item>
                                <Form.Item label={<img src="./assets/freelancer_pro/fb.png" alt=".." height="100%"/>} >
                                    <Input placeholder="Facebook Account"/>
                                </Form.Item>
                                <Form.Item label={<img src="./assets/freelancer_pro/telegram.png" alt=".." height="100%"/>} >
                                    <Input placeholder="Telegram Account"/>
                                </Form.Item>
                                <Form.Item label={<img src="./assets/freelancer_pro/twiter.png" alt=".." height="100%"/>} >
                                    <Input placeholder="Twitter Account"/>
                                </Form.Item>
                                <Form.Item label={<img src="./assets/freelancer_pro/linkin.png" alt=".." height="100%"/>} >
                                    <Input placeholder="LinkIn Account"/>
                                </Form.Item>
                                <div className="my-5">
                                    <Button className="cancel" variant="warning">Cancel</Button>
                                    <Button variant="warning" className="float-end">Update</Button>
                                </div>
                            </Col>
                        </Row>
                    </TabPane>
                    <TabPane tab="Company Information" key="3">
                        <Row className="mt-4">
                            <Col lg={5} md={5}>
                                <img src="./assets/freelancer_pro/cover-edit.png" width="100%" alt=".."/>
                            </Col>
                            <Col lg={7} md={7}>
                                <h5 className="mb-4">Company Information</h5>
                                <Row>
                                    <Col lg={6} md={6}><UploadImg /></Col>
                                    <Col lg={6} md={6}>
                                        <h6>Company Information</h6>
                                        <TextArea placeholder="- company info" style={{height:'12em'}}/>
                                        <div className="my-5">
                                            <Button className="cancel" variant="warning">Cancel</Button>
                                            <Button variant="warning" className="float-end">Update</Button>
                                        </div>
                                    </Col>
                                </Row>          
                            </Col>
                        </Row>
                    </TabPane>
                    <TabPane tab="Account Setting" key="4">
                        <Row className="mt-4">
                            <Col lg={5} md={5}>
                                <img src="./assets/freelancer_pro/cover-edit.png" width="100%" alt=".."/>
                            </Col>
                            <Col lg={7} md={7}>
                            <Tabs type="card">
                                <TabPane tab="Deactivate Account" key="4.1">
                                    <h5 className="my-4">Deactivate Account</h5>
                                    <Form.Item 
                                    label="Email"
                                    name="email"
                                    rules={[{ required: true}]}
                                    >
                                        <Input prefix={<MailOutlined className="site-form-item-icon" />} placeholder="Email"/>
                                    </Form.Item>
                                    <Form.Item
                                        label="Password"
                                        name="password"
                                        rules={[{ required: true}]}
                                    >
                                        <Input.Password prefix={<LockOutlined className="site-form-item-icon" />} placeholder="Password"/>
                                    </Form.Item>
                                    <div className="float-end">
                                        <a className="login-form-forgot" href=" ">
                                            Forgot password ?
                                        </a>
                                    </div><br/>
                                    <div className="my-4 ps-4">
                                        <Button className="cancel" variant="warning">Cancel</Button>
                                        <Button variant="danger" className="float-end">Deactivate</Button>
                                    </div>
                                </TabPane>
                                <TabPane tab="Reset Password " key="4.2" className="reset-ps">
                                    <h5 className="my-4">Reset Password</h5>
                                    <Form.Item
                                        label="Old Password"
                                        name="oldPassword"
                                        rules={[{ required: true}]}
                                    >
                                        <Input.Password prefix={<LockOutlined className="site-form-item-icon" />} placeholder="Enter old password"/>
                                    </Form.Item>
                                    <Form.Item
                                        label="New Password"
                                        name="newPassword"
                                        rules={[{ required: true}]}
                                    >
                                        <Input.Password prefix={<LockOutlined className="site-form-item-icon" />} placeholder="Enter new password"/>
                                    </Form.Item>
                                    <Form.Item
                                        label="Confirm Password"
                                        name="conPassword"
                                        rules={[{ required: true}]}
                                    >
                                        <Input.Password prefix={<LockOutlined className="site-form-item-icon" />} placeholder="Please confirm new password"/>
                                    </Form.Item>
                                    <div className="my-5 ps-1">
                                        <Button className="cancel" variant="warning">Cancel</Button>
                                        <Button variant="warning" className="float-end">Update</Button>
                                    </div>
                                </TabPane>
                            </Tabs>
                            </Col>
                        </Row>
                    </TabPane>
                </Tabs>
            </Container>
        </div>
    )
}

export default EditeProfile
