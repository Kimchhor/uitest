import React from 'react'
import './styles.css'
import { Col, Container, Row } from 'react-bootstrap'
import CardProject from '../../components/BusinessOwner/CardProject'
import {NavLink} from 'react-router-dom'


function ProfileDetail() {
    const a = [1,2,3,4,5];
    return (
        <div className="profile-detail bs-profile">
            <div className="cover">
                <img src="./assets/freelancer_pro/cover.png" alt="no cover show" width="100%"/>
            </div>
            <Container>
                <Row>
                    <Col lg={2} md={2}>
                        <div className="profile">
                            <img src="./assets/business_img/keo.png" alt=".." />
                        </div>
                    </Col>
                    <Col lg={8} md={8} className="mt-3">
                        <h4>Kay keo</h4>
                        <p style={{color:'#47444B'}}>Hello there . I’m Kay keo . I’m currently working at an IIT in the position of a Director.</p>
                    </Col>
                    <Col lg={2} md={2} className="mt-3">
                        <NavLink to="/profile_edit">
                            <img className="float-end" src="./assets/freelancer_pro/setting.png" alt=".."/>
                        </NavLink>
                    </Col>
                </Row>
                {/* ............... */}
                <Row className="mt-5">
                    <Col lg={4} md={4}>
                        <h5 className="mb-3">Contact Information</h5>
                        <div className="contact-info">
                            <div className="d-flex">
                                <img className="float-end me-4" src="./assets/freelancer_pro/phone.png" height="35px" alt=".."/>
                                <p><b>Telephone</b><br/>012 123 321 / 091 212 342</p>
                            </div>
                            <div className="d-flex">
                                <img className="float-end me-4" src="./assets/freelancer_pro/mail.png" height="35px" alt=".."/>
                                <p><b>Official Email</b><br/>traysengtit32@gmail.com</p>
                            </div>
                            <div className="d-flex">
                                <img className="float-end me-4" src="./assets/freelancer_pro/location.png" height="35px" alt=".."/>
                                <p><b>Address</b><br/>Phnom Penh , Cambodia </p>
                            </div>
                            <div className="d-flex">
                                <img className="float-end me-4" src="./assets/freelancer_pro/nation.png" height="35px" alt=".."/>
                                <p><b>Nationality</b><br/>Khmer</p>
                            </div>
                            <div className="socail-medai">
                                <img src="./assets/freelancer_pro/fb.png" alt="."/>
                                <img src="./assets/freelancer_pro/linkin.png" alt="."/>
                                <img src="./assets/freelancer_pro/telegram.png" alt="."/>
                                <img src="./assets/freelancer_pro/twiter.png" alt="."/>
                            </div>
                        </div>
                        {/* -------------Company Infomation----------------- */}
                        <h5 className="mb-3 mt-4">Company Infomation</h5>
                        <div className="company-info">
                            <img src="./assets/freelancer_pro/com-info.png" alt="company cover" width="100%"/>
                            <div className="des">
                                <h6 className="mb-4">Institute of Information Technology</h6>
                                <p>
                                A business description gives a snapshot of the business you plan to run or are already running.Business descriptions are typically written to appeal to potential investors, but they are important regardless of whether you're looking for funding. The size of a business description can vary and depends on a number of factors, including whether you're seeking funding, the types of products and services you're offering, your industry and the length of your business plan. Entrepreneur.com suggests keeping business descriptions direct and concise.
                                </p>
                            </div>
                        </div>
                    </Col>
                    {/* <Col lg={1} md={1}></Col> */}
                    <Col lg={8} md={8}>
                        <div className="project-list">
                                {a.map((value) => {                                                              
                                        return  (
                                            <>
                                                <CardProject/>
                                            </>
                                        )
                                    }
                                )}<br/>
                        </div>
                    </Col>
                </Row>
            </Container>
        </div>
    )
}

export default ProfileDetail
