import React from 'react'
import { BrowserRouter as Router , Route ,Switch } from 'react-router-dom'

// ----------Component--------------
import NavMenu from '../../components/BusinessOwner/NavMenu'
import Feed from './Feed'
import Footer from '../../components/Footer'
import PostProjectDetail from './PostProjectDetail'
import ProfileDetail from './ProfileDetail'
import PostProject from './PostProject'
import EditeProfile from './EditeProfile'
import SearchFreelancer from './SearchFreelancer'
import FreeCardDetail from './FreeCardDetail'
import FreeProfileDetail from './FreeProfileDetail'
import AboutUs from '../AboutUs'

function BusinessOwner() {
    return (
        <div>
            <Router>
                <NavMenu/>
                <Switch>
                    <Route path="/project_detail">
                        <PostProjectDetail />
                    </Route>    
                    <Route path="/profile_detail">
                        <ProfileDetail/>
                    </Route>
                    <Route path="/post_project">
                        <PostProject/>
                    </Route>
                    <Route path="/profile_edit">
                        <EditeProfile/>
                    </Route>
                    <Route path="/search_freelancer">
                        <SearchFreelancer />
                    </Route>
                    <Route path="/freelancer_card_detail">
                        <FreeCardDetail />
                    </Route>
                    <Route path="/freelancer_profile_detail">
                        <FreeProfileDetail />
                    </Route>
                    <Route path="/aboutus">
                        <AboutUs />
                    </Route>
                    <Route path="/">
                        <Feed />
                    </Route>
                </Switch>
                <Footer />
            </Router>
        </div>
    )
}

export default BusinessOwner