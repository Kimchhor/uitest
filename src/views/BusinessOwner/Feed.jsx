import React from 'react';
import './styles.css';
import { Container, Button, Row, Col} from 'react-bootstrap';
import { Tabs } from 'antd';
import CardProject from '../../components/BusinessOwner/CardProject';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import { NavLink } from 'react-router-dom';


const { TabPane } = Tabs;
const Feed = () => {
    const a=[1,2,3,4,5,6]
    return (
        <div className="bu-feed">
            <Container>
                <Row className="my-5">
                    <Col lg={6} md={6}>
                        <h2 className="mt-5">Welcome Keo !</h2>
                        <p className="my-4">Here is your projects dashboard where you can create and manage all of your project posts. Your project post status will be categoried in three categories such draft, visible and invisible.  </p>
                        <NavLink to="/post_project">
                            <Button variant="warning">Post Project</Button>
                        </NavLink>
                    </Col>
                    <Col lg={6} md={6}>
                        <img src="./assets/business_img/cover-img.png" width="100%" alt="cover" />
                    </Col>
                </Row>
                {/* Categories Owl carousel */}
                <h5 className="mb-5">Explore the popular categories</h5>
                <div className="categories">
                    <NavLink to="/search_freelancer">
                        <OwlCarousel
                            className="owl-theme"
                            loop margin={10}
                            nav={true}
                            dots={false}
                            items={5}
                        >
                            
                                <div class="item">
                                    <img src="./assets/business_img/cat1.png" alt="no img show" />
                                </div>
                                <div class="item">
                                    <img src="./assets/business_img/cat2.png" alt="no img show" />
                                </div>
                                <div class="item">
                                    <img src="./assets/business_img/cat3.png" alt="no img show" />
                                </div>
                                <div class="item">
                                    <img src="./assets/business_img/cat4.png" alt="no img show" />
                                </div>
                                <div class="item">
                                    <img src="./assets/business_img/cat5.png" alt="no img show" />
                                </div>
                                <div class="item">
                                    <img src="./assets/business_img/cat2.png" alt="no img show" />
                                </div>
                                <div class="item">
                                    <img src="./assets/business_img/cat3.png" alt="no img show" />
                                </div>
                        </OwlCarousel>
                    </NavLink>
                </div>
                <h5 className="my-5">Project Dashboard</h5>
                <Tabs type="card">
                    <TabPane tab="Project" key="1">
                    {a.map((value) => {                                                              
                            return  (
                                <><CardProject/><br/></>
                            )
                        }
                    )}
                    </TabPane>
                    <TabPane tab="Pending" key="2" className="pending">
                        {a.map((value) => {                                                              
                                return  (
                                    <><CardProject/><br/></>
                                )
                            }
                        )}
                    </TabPane>
                    <TabPane tab="Draft" key="3" className="draft">
                        {a.map((value) => {                                                              
                                return  (
                                    <><CardProject/><br/></>
                                )
                            }
                        )}
                        
                    </TabPane>
                </Tabs>
            </Container>
        </div>
    );
};

export default Feed;